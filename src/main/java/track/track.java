package track;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class track {
	static String				temp;
	//currency exchange values from xe.com
	static String[][] currencyExchange = {{"USD", "1.000000000"},{"EUR","0.8478653835"},{"GBP","0.7461065092"},{"INR","67.4355757970"},{"AUD","1.3129775941"},{"CAD","1.2996298936"},{"SGD","1.3337859397"},{"CHF","0.9838394897"},{"MYR","3.9907228794"},{"JPY","110.2056741867"},{"CNY","6.4048789302"},{"NZD","1.4215902877"},{"THB","32.0912103539"},{"HUF","272.9327285787"},{"AED","3.6725000000"},{"HKD","7.8467077164"},{"MXN","20.6248621053"},{"ZAR","13.2067013976"},{"PHP","53.1196273181"},{"SEK","8.6483026642"},{"IDR","13936.9888760820"},{"SAR","3.7500000000"},{"BRL","3.7103943565"},{"TRY","4.5527635491"},{"KES","101.1181605188"},{"KRW","1075.3062306908"},{"EGP","17.8692485392"},{"IQD","1187.9517918412"},{"NOK","8.0156790695"},{"KWD","0.3026649775"},{"RUB","62.7565331580"},{"DKK","6.3166819661"},{"PKR","117.3321721184"},{"ILS","3.5809256059"},{"PLN","3.6268834016"},{"QAR","3.6400000000"},{"XAU","0.0007704187"},{"OMR","0.3845000000"},{"COP","2858.3830426809"},{"CLP","633.3061139752"},{"TWD","29.8477724080"},{"ARS","26.0586777163"},{"CZK","21.7375646855"},{"VND","22813.5544718050"},{"MAD","9.4413568882"},{"JOD","0.7090000000"},{"BHD","0.3760000000"},{"XOF","556.1632333345"},{"LKR","159.4784030845"},{"UAH","26.0996062472"},{"NGN","360.7883730975"},{"TND","2.5632929673"},{"UGX","3822.9449497322"},{"RON","3.9514225244"},{"BDT","84.2135956748"},{"PEN","3.2653203335"},{"GEL","2.4537264236"},{"XAF","556.1632333345"},{"FJD","2.0634337109"},{"VEF","9.9875000000"},{"BYN","2.0019000954"},{"HRK","6.2589459951"},{"UZS","7944.7289936283"},{"BGN","1.6582805529"},{"DZD","116.8612683256"},{"IRR","42241.9292146720"},{"DOP","49.5985733785"},{"ISK","105.9040617742"},{"XAG","0.0593323346"},{"CRC","566.8868546365"},{"SYP","515.0344641003"},{"LYD","1.3543753628"},{"JMD","129.0363063159"},{"MUR","34.0119019348"},{"GHS","4.7333833164"},{"AOA","238.0036320559"},{"UYU","31.3689243473"},{"AFN","71.2996193539"},{"LBP","1507.5000000000"},{"XPF","101.1772533951"},{"TTD","6.7285106368"},{"TZS","2280.6097270591"},{"ALL","108.2926875652"},{"XCD","2.7024707277"},{"GTQ","7.4531349104"},{"NPR","108.4026880937"},{"BOB","6.9119396116"},{"ZWD","361.9000000000"},{"BBD","2.0000000000"},{"CUC","1.0000000000"},{"LAK","8348.3322162006"},{"BND","1.3337859397"},{"BWP","10.1557249166"},{"HNL","23.8153026767"},{"PYG","5653.8198176661"},{"ETB","27.4944683707"},{"NAD","13.2067013976"},{"PGK","3.2555191120"},{"SDG","17.9999994702"},{"MOP","8.0821089479"},{"NIO","31.6014473166"},{"BMD","1.0000000000"},{"KZT","334.4228276040"},{"PAB","1.0000000000"},{"BAM","1.6582805529"},{"GYD","208.2383711376"},{"YER","250.1482820967"},{"MGA","3362.2418746552"},{"KYD","0.8200000165"},{"MZN","59.2324261587"},{"RSD","100.2453011745"},{"SCR","13.4687673394"},{"AMD","483.2071040523"},{"SBD","7.8795374876"},{"AZN","1.7012305752"},{"SLL","7903.7988708309"},{"TOP","2.2333760634"},{"BZD","2.0073131688"},{"MWK","738.5180964148"},{"GMD","47.1964828686"},{"BIF","1761.5436934562"},{"SOS","575.2512285351"},{"HTG","64.7660725140"},{"GNF","9025.9537711013"},{"MVR","15.5213768576"},{"MNT","2411.5870175248"},{"CDF","1607.4091628158"},{"STD","20803.8622415910"},{"TJS","9.0695537558"},{"KPW","900.0004650203"},{"MMK","1358.9933280990"},{"LSL","13.2067013976"},{"LRD","139.0605630420"},{"KGS","68.4527818910"},{"GIP","0.7461065092"},{"XPT","0.0011032482"},{"MDL","16.7482439038"},{"CUP","26.5000000000"},{"KHR","4053.8440846909"},{"MKD","52.1093527201"},{"VUV","109.6260496808"},{"MRO","355.6228732269"},{"ANG","1.7923269575"},{"SZL","13.2067013976"},{"CVE","93.4941158335"},{"SRD","7.4654998940"},{"XPD","0.0009788939"},{"SVC","8.7500000000"},{"BSD","1.0000000000"},{"XDR","0.7039056281"},{"RWF","869.2779533188"},{"AWG","1.7900000000"},{"DJF","177.8054044474"},{"BTN","67.4355757970"},{"KMF","417.1224250008"},{"WST","2.5374758213"},{"SPL","0.1666666660"},{"ERN","15.0000000000"},{"FKP","0.7461065092"},{"SHP","0.7461065092"},{"JEP","0.7461065092"},{"TMT","3.5000000298"},{"TVD","1.3129775941"},{"IMP","0.7461065092"},{"GGP","0.7461065092"},{"ZMW","10.1810808072"}};
	static ArrayList<String>	tempCurrency	= new ArrayList<>();
	static ArrayList<Integer>	tempValue		= new ArrayList<>();
	static int					counter = 0;
	static long					millibefore;
	static long					milliafter;
	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println("Enter value:");
		//Thread for sum
		Thread thread = new Thread() {
			public void run() {
				ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
				exec.scheduleAtFixedRate(new Runnable() {
				  public void run() {
							sumValues(tempCurrency);
				  }
				}, 0, 60, TimeUnit.SECONDS);
			}
		};
		thread.run();
		Scanner read = new Scanner(System.in);
		//Loop until "quit"
		while(true) {
			temp = read.nextLine();
			if(temp.equals("quit")) {
				System.exit(1);
			}
			else if (temp.contains(" ") && temp.length()>4 && !temp.startsWith(" ") && !temp.endsWith(" ")) {
				if(split(temp)[0].length()==3 && isNumber(split(temp)[1])) {
					try {
						tempValue.add(Integer.parseInt(split(temp)[1]));
						tempCurrency.add(split(temp)[0].toUpperCase());
					}
					catch (NumberFormatException nfe) {
						System.out.println("Wrong number format");
					}
				}
				else if(split(temp)[0].length()!=3) {
					System.out.println("Wrong currency format");
				}
				else if(!isNumber(split(temp)[1])) {
					System.out.println("Not a number");
				}
			}
			else {
				System.out.println("Something seems wrong here");
			}
		}
	}
	public static boolean isNumber(String string) {  
		try {  
			double d = Double.parseDouble(string);  
		}  
		catch(NumberFormatException nfe) {  
			return false;  
		}  
	return true;
	}
	public static void sumValues(ArrayList array) {
			String[] unique = null;
			if(array.size() != 0) {
				unique = Arrays.stream(array.toArray()).distinct().toArray(String[]::new);
				for(int i = 0; i < unique.length; i++) {
					int numbers = 0;
					for(int j = 0; j < array.size(); j++) {
						if(tempCurrency.get(j).equals(unique[i])) {
							numbers += tempValue.get(j);
						}
					}
					if(numbers!=0) {
						System.out.println(unique[i]+ " " +numbers+ ((!unique[i].equals("USD") && returnIndex(unique[i]) != 0) ?  " (USD " +new DecimalFormat("##.##").format((double) (numbers/Float.parseFloat(currencyExchange[returnIndex(unique[i])][1])))+ ")" : ""));
					}
				}
			}
	}
	public static int returnIndex(String unique) {
		int index = 0;
		for(int i = 0; i < currencyExchange.length; i++) {
			if(unique.equals(currencyExchange[i][0])) {
				index = i;
			}
		}
		return index;
	}
	public static String[] split(String input) {
		String[] splitted;
		splitted = input.split("\\s+");
		return splitted;
	}
}