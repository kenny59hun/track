# readme

# Included jar file downloadable from Downloads. (I think this way it is easier to test)

Instructions to run:

if java path added to windows paths:

*		open Command Prompt
*		java -jar "path-to-file\track.jar"

if not added:

*		locate java folder and remember it
*		open Command Prompt
*		path-to-java-file -jar "path-to-file\track.jar"

Assumptions:
* if Currency is not 3 letters -> error message
* if Value is not a number or not integer (in the description it didn't say to make to use decimal points too) -> error message
* if Currency is different from the ones i found on xe.com -> still works